﻿namespace ORMlite
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Conect = new System.Windows.Forms.Button();
            this.C = new System.Windows.Forms.Button();
            this.R = new System.Windows.Forms.Button();
            this.U = new System.Windows.Forms.Button();
            this.D = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Conect
            // 
            this.Conect.Location = new System.Drawing.Point(264, 62);
            this.Conect.Name = "Conect";
            this.Conect.Size = new System.Drawing.Size(75, 23);
            this.Conect.TabIndex = 0;
            this.Conect.Text = "Conect";
            this.Conect.UseVisualStyleBackColor = true;
            this.Conect.Click += new System.EventHandler(this.button1_Click);
            // 
            // C
            // 
            this.C.Location = new System.Drawing.Point(85, 209);
            this.C.Name = "C";
            this.C.Size = new System.Drawing.Size(75, 23);
            this.C.TabIndex = 1;
            this.C.Text = "Create";
            this.C.UseVisualStyleBackColor = true;
            this.C.Click += new System.EventHandler(this.C_Click);
            // 
            // R
            // 
            this.R.Location = new System.Drawing.Point(241, 209);
            this.R.Name = "R";
            this.R.Size = new System.Drawing.Size(75, 23);
            this.R.TabIndex = 2;
            this.R.Text = "Read";
            this.R.UseVisualStyleBackColor = true;
            // 
            // U
            // 
            this.U.Location = new System.Drawing.Point(385, 209);
            this.U.Name = "U";
            this.U.Size = new System.Drawing.Size(75, 23);
            this.U.TabIndex = 3;
            this.U.Text = "Update";
            this.U.UseVisualStyleBackColor = true;
            // 
            // D
            // 
            this.D.Location = new System.Drawing.Point(521, 209);
            this.D.Name = "D";
            this.D.Size = new System.Drawing.Size(75, 23);
            this.D.TabIndex = 4;
            this.D.Text = "Delete";
            this.D.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 440);
            this.Controls.Add(this.D);
            this.Controls.Add(this.U);
            this.Controls.Add(this.R);
            this.Controls.Add(this.C);
            this.Controls.Add(this.Conect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Conect;
        private System.Windows.Forms.Button C;
        private System.Windows.Forms.Button R;
        private System.Windows.Forms.Button U;
        private System.Windows.Forms.Button D;
    }
}


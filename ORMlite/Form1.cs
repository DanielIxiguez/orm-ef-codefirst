﻿using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ORMlite
{
    public partial class Form1 : Form
    {
        OrmLiteConnectionFactory dbFactory = new OrmLiteConnectionFactory("ormlitetest.db",SqliteDialect.Provider);
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<Persona>();
            }
            MessageBox.Show("Done");
        }

        private void C_Click(object sender, EventArgs e)
        {
            using (var db = dbFactory.Open())
            {
                Persona persona = new Persona() {apellido= "Iñigiez Median",nombre="Edgar Daniel"};
                db.Insert(persona);
                MessageBox.Show("Done");
            }
            }
    }
}
